import React, { useState, useEffect } from 'react';
import './styles.css';

import SimpleBarReact from 'simplebar-react';
import 'simplebar/src/simplebar.css';

// import targetImgs from '../assets/json/target2.json';
// import targetImgs from '../assets/json/targetMini.json';
import targetImgs from '../assets/json/targetMini2.json';
import SimilarItems from '../similar';

const Home = () => {
	const mainItemsRef = React.createRef();
	const innerWidth = window.innerWidth;

	let size = 100;

	let sessionPageNum = sessionStorage.getItem('pageNum') ? JSON.parse(sessionStorage.getItem('pageNum')) : 0;
	// let globalData = targetImgs;

	const [ targetData, setTargetData ] = useState([]);
	const [ similarData, setSimilarData ] = useState(null);
	const [ similarId, setSimilarId ] = useState('');
	const [ activeIndex, setActiveIndex ] = useState(0);

	const showSimilarTo = (el, id, i) => {
		setActiveIndex(i);
		setSimilarData(el);
		setSimilarId(id);
		mainItemsRef.current.scrollTo({ top: innerWidth * 0.265 * i, behavior: 'smooth' });
	};

	const renderHomeCard = targetData.map((el, i) => {
		let id = Object.keys(el)[1];
		return (
			<div
				key={`homeImage${i}`}
				onClick={() => showSimilarTo(el, id, i)}
				className='TS-images-section-outer-wrapper'>
				<img
					className={activeIndex === i ? 'TS-images-active homeImage' : 'TS-images homeImage'}
					src={el[id].post.imageUrl}
					alt=''
				/>
			</div>
		);
	});

	useEffect(
		() => {
			if (targetData.length === 0) {
				let start, end;
				let pageNo = JSON.parse(sessionStorage.getItem('pageNum'));
				if (!pageNo) {
					sessionStorage.setItem('pageNum', 0);
					start = 0;
					end = size;
				} else {
					start = pageNo * size;
					end = start + size;
				}
				let tData = [ ...targetImgs ];
				let tempData = tData.slice(start, end);
				setPaginatedData(tempData, start, end)
			}
		},
		[ targetData.length, size ]
	);

	const paginate = page => {
		let prevPage = JSON.parse(sessionStorage.getItem('pageNum'));
		if (page === 1 ? prevPage < 4 : prevPage !== 0) {
			sessionStorage.setItem('pageNum', prevPage + page);
			sessionPageNum = sessionPageNum + page
			let start = sessionPageNum * size,
				end = start + size;
			let tempData = targetImgs.slice(start, end);
			setPaginatedData(tempData, start, end)
		}
	}

	const setPaginatedData = (tempData, start, end) => {
		setTargetData(targetImgs.slice(start, end));
		setSimilarData(tempData[0]);
		setSimilarId(Object.keys(tempData[0])[1]);
		setActiveIndex(0)
		mainItemsRef.current.scrollTo({ top: 0 });
	}

	return (
		<div className='homeContainer'>
			<SimpleBarReact scrollableNodeProps={{ ref: mainItemsRef }} className='simpleBarHome'>
				{renderHomeCard}
				<div className='TS-images-section-outer-wrapper-btn'>
					<button
						disabled={sessionPageNum === 0 ? true : false}
						style={sessionPageNum === 0 ? { opacity: '0.5' } : { opacity: '1' }}
						id='PS-inner-container-btn-prev'
						className='PS-inner-container-btn'
						onClick={(e) => {
							e.persist();
							paginate(-1);
						}}>
						Prev
					</button>
					<button
						disabled={+sessionStorage.getItem('pageNum') === 4 ? true : false}
						style={+sessionStorage.getItem('pageNum') === 4 ? { opacity: '0.5' } : { opacity: '1' }}
						className='PS-inner-container-btn'
						onClick={(e) => {
							e.persist();
							paginate(1);
						}}>
						Next
					</button>
				</div>
			</SimpleBarReact>
			<SimilarItems data={similarData} id={similarId} />
		</div>
	);
};

export default Home;
