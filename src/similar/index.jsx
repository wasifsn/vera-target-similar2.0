import React, { useEffect } from 'react';

import SimpleBarReact from 'simplebar-react';
import 'simplebar/src/simplebar.css';

import SimilarCard from "./similarCard"

const SimilarItems = ({ data, id }) => {
	const similarTopsRef = React.createRef();
	const similarBottomsRef = React.createRef();

	useEffect(
		() => {
			if (similarTopsRef.current) similarTopsRef.current.scrollTop = 0;
			if (similarBottomsRef.current) similarBottomsRef.current.scrollTop = 0;
		},
		[similarBottomsRef, similarTopsRef]
	);

	const getSimilarData = type => {
		return (
			data[id][`similar_${type}`] && (
			<div className='similar-cloth-container'>
				<div className='similar-header'>{`Similar ${type}`}</div>
				<SimpleBarReact scrollableNodeProps={{ ref: type === "bottoms" ? similarBottomsRef : similarTopsRef  }} className="simpleBarSimilar similar-items-container">
					<div className='similar-items-container'>
						{data[id][`similar_${type}`].map((el, i) => <SimilarCard type={type} data={el} index={i}/>)}
					</div>
				</SimpleBarReact>
			</div>
		));
	};

	return (
		<div className='similar-container'>
			{data !== null && (
				<div className='app-container'>
					{getSimilarData('tops')}
					{getSimilarData('bottoms')}		
				</div>
			)}
		</div>
	);
};

export default SimilarItems;
