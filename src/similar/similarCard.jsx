import React from 'react';

const SimilarCard = ({ type, data, index }) => {

	const decodeString = str =>
		str
			.replace(/&#153;/g, '™')
			.replace(/&#8482;/g, '™')
			.replace(/&#39;/g, "'")
			.replace(/&#38;/g, '&')
			.replace(/&#174;/g, '®');

	return (
		<div key={`similarItem${type}${index}`} className='similar-items-inner-container'>
            <img
                className='TS-images similarImage'
                onError={e => e.target.parentNode.style.display = 'none'}
                src={data.similarUrl}
                alt = ""
            />
            <div className='similar-items-inner-details-container'>
                <p className='similar-item-name'>{decodeString(data.name).slice(0, 60)}...</p>
                <p className='similar-item-price'>$ {data.price['$numberInt']}.99</p>
            </div>
        </div>
	);
};

export default SimilarCard;
